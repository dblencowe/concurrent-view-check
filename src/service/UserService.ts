import User from "../models/User"

export interface UserServiceInterface {
  findUserById(cognitoId: string): User | undefined
  createUser(cognitoId: string): User
}

export default class UserService implements UserServiceInterface {
  private users: User[] = []

  public findUserById (cognitoId: string) {
    return this.users.find((user) => user.cognitoId === cognitoId)
  }

  public createUser (cognitoId: string): User {
    const user = new User(cognitoId)
    this.users.push(user)

    return user
  }
}