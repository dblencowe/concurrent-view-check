# Example Curl Requests

## Begin Viewing Product
```shell
curl --request POST \
  --url http://localhost:8008/ \
  --header 'content-type: application/json' \
  --data '{
  "event": {
    "path": "/user/start-viewing",
    "httpMethod": "POST",
    "requestContext": {
      "identity": {
        "cognitoAuthenticationProvider": "cognito-idp.us-east-1.amazonaws.com/us-east-1_XXXXXXXX,cognito-idp.us-east-1.amazonaws.com/us-east-1_XXXXXXXX:CognitoSignIn:a9f81009-46e3-4081-95b7-343ac8713cf3"
      }
    },
    "body": "{\"productId\":\"fd03f334-3cac-4fd1-acae-3418a715ad02\"}"
  }
}'
```

## Access Invalid endpoint
```shell
curl --request POST \
  --url http://localhost:8008/ \
  --header 'content-type: application/json' \
  --data '{
  "event": {
    "path": "/just-something-random",
    "httpMethod": "POST",
    "requestContext": {
      "identity": {
        "cognitoAuthenticationProvider": "cognito-idp.us-east-1.amazonaws.com/us-east-1_XXXXXXXX,cognito-idp.us-east-1.amazonaws.com/us-east-1_XXXXXXXX:CognitoSignIn:a9f81009-46e3-4081-95b7-343ac8713cf3"
      }
    },
    "body": ""
  }
}'
```