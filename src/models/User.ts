export interface Stream {
  productId: string
}


export default class User {
  public readonly cognitoId: string
  private streams: Stream[] = []

  public constructor(cognitoId: string) {
    this.cognitoId = cognitoId
  }

  getStreams (): Stream[] {
    return this.streams
  }

  addStream (productId: string): Stream[] {
    this.streams.push({
      productId
    })

    return this.streams
  }
}