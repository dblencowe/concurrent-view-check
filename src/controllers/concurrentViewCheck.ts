import { APIResponse, response } from "../helpers";
import { UserServiceInterface } from "../service/UserService";

export default function (userService: UserServiceInterface, cognitoId: string, productId: string): APIResponse {
  let user = userService.findUserById(cognitoId)
  if (!user) {
    user = userService.createUser(cognitoId)
  }

  if (user.getStreams().length >= 3) {
    return response(403, 'Max content reached')
  }

  user.addStream(productId)

  return response(201)
}