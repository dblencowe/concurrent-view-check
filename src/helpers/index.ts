export interface APIResponse {
  statusCode: number
  headers: { [header: string]: string }
  body?: string
}

export function getCognitoIdFromEvent (cognitoAuthenticationProvider: string): string {
  return cognitoAuthenticationProvider.split(':').pop() as string
}

export function response (statusCode, body = '', additionalHeaders: { [key: string]: string } = {}): APIResponse {
  const defaultHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*'
  }

  return {
    statusCode: statusCode,
    body: JSON.stringify(body),
    headers: {
      ...defaultHeaders,
      ...additionalHeaders
    }
  }
}