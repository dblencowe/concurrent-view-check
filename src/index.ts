import concurrentViewCheck from "./controllers/concurrentViewCheck"
import { APIResponse, getCognitoIdFromEvent, response } from "./helpers"
import UserService, { UserServiceInterface } from "./service/UserService"

enum HTTPMethod {
  POST
}

export interface APIGatewayEvent {
  path: string
  httpMethod: HTTPMethod
  requestContext: {
    identity: {
      cognitoAuthenticationProvider: string
    }
  }
  body: string
}

const MissingProductIdException = 'MissingProductId'

class BadRequestMissingProductIdException extends Error {
  public readonly type = MissingProductIdException
  public readonly message = 'Product ID missing from body'
  constructor () {
    super()
  }
}


const userService = new UserService()

export function createStartViewingHandler (userService: UserServiceInterface) {
  return (event: APIGatewayEvent): APIResponse => {
    try {
      const cognitoId = getCognitoIdFromEvent(event.requestContext.identity.cognitoAuthenticationProvider)
      if (!cognitoId) {
        return response(401, 'Unauthorised')
      }
    
      const body = JSON.parse(event.body)
      const productId = body.productId
      if (!productId) throw new BadRequestMissingProductIdException()

      return concurrentViewCheck(userService, cognitoId, productId)
    } catch (e: any) {
        if (e.type === MissingProductIdException) {
          return response(400, e.message)
        }
        console.error(e)

        return response(500)
    }   
  }
}

export const handler = createStartViewingHandler(new UserService()) 