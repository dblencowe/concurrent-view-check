import concurrentViewCheck from '../../src/controllers/concurrentViewCheck'
import { v4 as uuid } from 'uuid'
import { mock } from 'jest-mock-extended'
import UserService, { UserServiceInterface } from '../../src/service/UserService'
import User from '../../src/models/User'

test('given a valid product id and a new cognito ID, when the stream is successfully added the controller returns a 201', () => {
  const cognitoId = uuid()
  const productId = uuid()
  const mockUserService = mock<UserServiceInterface>()
  mockUserService.findUserById.mockReturnValueOnce(undefined)
  mockUserService.createUser.mockReturnValueOnce(new User(cognitoId))
  const response = concurrentViewCheck(mockUserService, cognitoId, productId)
  expect(response.statusCode).toBe(201)
})

test('given a set of 4 streams to start, when the fourth stream is attempted to be added, the controller returns a 403 (Operation forbidden) and the stream is not added', () => {
  const newCognitoId = uuid()
  const mockUserService = mock<UserServiceInterface>()
  mockUserService.findUserById.mockReturnValue(new User(newCognitoId))
  let response
  for (let i = 0; i <= 4; i++) {
    const productId = uuid()
    response = concurrentViewCheck(mockUserService, newCognitoId, productId)
  }
  expect(response.statusCode).toBe(403)
})