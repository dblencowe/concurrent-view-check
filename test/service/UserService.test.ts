import UserService from '../../src/service/UserService'
import { v4 as uuid } from 'uuid'

const cognitoId = uuid()

test('given a valid UserService instance, when supplied with a valid User ID we can create a user', () => { 
  const userService = new UserService() 
  const user = userService.createUser(cognitoId)
  expect(user.cognitoId).toBe(cognitoId)
})

test('given a valid UserService instance, when supplied with a valid User ID we can find the user within the backend', () => {
  const userService = new UserService() 
  userService.createUser(cognitoId)
  const user = userService.findUserById(cognitoId)
  expect(user?.cognitoId).toBe(cognitoId)
})

test('given a valid UserService instance, when supplied with a invalid User ID we can not find the user within the database', () => {
  const userService = new UserService() 
  const newCognitoId = uuid()
  const user = userService.findUserById(newCognitoId)
  expect(user).toBe(undefined)
})