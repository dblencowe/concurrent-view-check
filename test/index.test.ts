import { APIGatewayEvent, handler } from '../src/index'
import { v4 as uuid } from 'uuid'

test('given a valid Start Viewing event, when the handler is called it returns a 201 status code', () => {
  const randomUser = uuid()
  const event = {
    requestContext: {
      httpMethod: 'POST',
      identity: {
        cognitoAuthenticationProvider: `cognito-idp.us-east-1.amazonaws.com/us-east-1_xxxxxxxx,cognito-idp.us-east-1.amazonaws.com/us-east-1_xxxxxxxx:CognitoSignIn:${randomUser}`
      }
    },
    path: '/user/start-viewing',
    body: "{\"productId\":\"fd03f334-3cac-4fd1-acae-3418a715ad02\"}"
  }

  const response = handler(event as unknown as APIGatewayEvent)
  expect(response.statusCode).toBe(201)
})

test('given an incorrectly formatted Identity string, when the handler is called the request is rejected with a 401 status code', () => {
  let event = {
    requestContext: {
      httpMethod: 'POST',
      identity: {
        cognitoAuthenticationProvider: 'cognito-idp.us-east-1.amazonaws.com/us-east-1_xxxxxxxx,cognito-idp.us-east-1.amazonaws.com/us-east-1_xxxxxxxx:CognitoSignIn:'
      }
    },
    path: '/user/start-viewing',
    body: "{\"productId\":\"fd03f334-3cac-4fd1-acae-3418a715ad02\"}"
  }

  const response = handler(event as unknown as APIGatewayEvent)
  expect(response.statusCode).toBe(401)
})

test('given a missing product id, when the stream is attempted to be added, the controller returns a 400 status code with an appropriate error message', () => {
  const randomUser = uuid()
  let event = {
    requestContext: {
      httpMethod: 'POST',
      identity: {
        cognitoAuthenticationProvider: `cognito-idp.us-east-1.amazonaws.com/us-east-1_xxxxxxxx,cognito-idp.us-east-1.amazonaws.com/us-east-1_xxxxxxxx:CognitoSignIn:${randomUser}`
      }
    },
    path: '/user/start-viewing',
    body: "{}"
  }

  const response = handler(event as unknown as APIGatewayEvent)
  expect(response.statusCode).toBe(400)
  expect(response.body).toBe(JSON.stringify('Product ID missing from body'))
})