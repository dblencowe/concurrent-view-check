import UserClass from '../../src/models/User'
import { v4 as uuid } from 'uuid'

test('given an instance of User, we are able to access a collection of Streams', () => {
  const cognitoId = uuid()  
  const user = new UserClass(cognitoId)
  expect(user instanceof UserClass).toBe(true)
  expect(user.getStreams.length).toBe(0)
})

test('given a valid instance of user, when we add a Stream it is retrievable from the user', () => {
  const cognitoId = uuid()  
  const user = new UserClass(cognitoId)
  const productId = uuid()
  user.addStream(productId)
  expect(user.getStreams().length).toBe(1)
  expect(user.getStreams()[0]).toStrictEqual({
    productId
  })
})