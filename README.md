# Concurrent View Check Service

## Setup
Before any of the below commands can be run you must first install the required Node Modules
```shell
npm install
```

## Tests
Several tests were written in Jest during the design of the lambda. They can be run with the following command:
```shell
npm run test
```

## Mock Server
I've included a script that uses the lambda-local package to allow for quick and easy testing via HTTP requests. To run it simply run
```shell
npm run mock
```

Example curl requests can be found in the `examples` directory of this repository

## The Implementation
The code is designed to be deployed as an AWS Lambda, and follows the same implmentation as an API Gateway integration, common in highly scaleable serverless architecture.

For the purposes of this demo the user data is stored in Memory although in a production environment the UserService and Model would be expanded to be backed by a more permanent data store with an emphasis on fast read / write time, such as DynamoDB or Redis.

### Assumptions
- Routing to the lambdas handler is supplied via API Gateway
- Authentication / Authorization is handled externally & the user is indicated on the Lambda Event
- Each User is represented by a unique GUID
- Each product is represented by a unique GUID, no further details about the product are needed by the service
- Another service is responsible for removing streams from the user, for example, when playback is finished or the user closes their player

### Logging
Logging for the lambda would be provided by AWS Cloudwatch, with custom metrics and alarms setup for catching Runtime Errors, Function run length anamolies and parsing of the CloudWatch log group for `console.error` calls.
The service could easily be included in an existing AWS Xray service map, or form part of its own.

### Scalability
Building this service as a Lambda provides several different available access patterns, many of which are designed to be used at high scale with the inclusion of other AWS Services.

#### Accessing the service via HTTP request
The service can easily be setup to be accessed via a HTTP request when configured using AWS Lambda and CloudFront. The existing code setup will accept User Authorization via the Identity provided with the request, which can be set by either Cognito or an Authorizer lambda making the implementation widely compatiable with any existing infrastructure

#### Accessing the services as part of a Lambda chain or Step Function

